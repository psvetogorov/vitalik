from aiogram import Bot, Dispatcher, types
from aiogram.types import Message


# Инициализируем бота и диспетчера
bot = Bot(token='6628058068:AAGfT5lDKIQJTzsHbWuXlXeOmW5h-AVcHPE')
dp = Dispatcher(bot)


def get_user_name(user):
    if user.first_name:
        return user.first_name
    return user.username

def get_welcome_message(user):
    text = f'Привет, {get_user_name(user)}!\n\n'
    text += 'Добро пожаловать в наш чат. Чтобы начать, пожалуйста, выполните следующие шаги:\n\n'
    text += '1. Представьтесь.\n'
    text += '2. Расскажите нам немного о себе.\n'
    text += '3. Ознакомьтесь с правилами чата в закрепленном сообщении.'
    return text

@dp.message_handler(content_types=types.ContentTypes.NEW_CHAT_MEMBERS)
async def greet_new_members(message: Message):
    print('here')
    # Получаем список новых участников чата.
    new_members = message.new_chat_members

    # Отправляем инструкции в личное сообщение каждому новому участнику.
    for member in new_members:
        # Отправляем инструкции в чат, в который пришло сообщение.
        await bot.send_message(message.chat.id, get_welcome_message(member))

        # Отправляем инструкции в личное сообщение каждому новому участнику.
        await bot.send_message(member.id, get_welcome_message(member))

# @dp.message_handler()
# async def greet_new_members(message: Message):
#     print(f'{message=}')
#     await message.reply('ECHO')


print('OK!')

# Запускаем бота.
if __name__ == "__main__":
    from aiogram import executor
    executor.start_polling(dp, skip_updates=False)
